''' 
  ____                 __    
 / __ \__ _____ ____  / /_   
/ /_/ / // / _ `/ _ \/ __/   
\___\_\_,_/\_,_/_//_/\__/    

| quant_math.py |

Contains the methods that contains the quantification equations. Current supported equations
include dNPAF, dNSAF, sum, and average. See documentation for equation details.
'''

import sys, traceback
import numpy as np

class QuantMath:
    def __init__(self, total_proteins=None):
        self.total_proteins = total_proteins
        self.all_unique_abundances = []
        self.all_shared_abundances = []
        self.all_unique_lengths = []
        self.all_shared_lengths = []

    #----------------------------------------------------------
    # Sum over precursor abundances determines quantification
    #----------------------------------------------------------
    @staticmethod
    def sum_abundances(unique_abundances=None, shared_abundances=None, unique_peptide_lengths=None, shared_peptide_lengths=None, protein_length=None):
        abund = 0.0
        if len(unique_abundances) == 0 and len(shared_abundances) == 0:
            return(0.0)
        
        abund = np.sum(unique_abundances) + np.sum(shared_abundances)
        
        if np.isnan(abund):
            err_msg = "Encountered nan values in sum equation"
            raise QuantMath_Error(message=err_msg)

        return(abund)

    #----------------------------------------------
    # Average abundance determines quantification
    #----------------------------------------------
    @staticmethod
    def avg_abundances(unique_abundances=None, shared_abundances=None, unique_peptide_lengths=None, shared_peptide_lengths=None, protein_length=None):
        abund = 0.0
        if len(unique_abundances) == 0 and len(shared_abundances) == 0:
            return(0.0)
        
        abund = (np.sum(unique_abundances) + np.sum(shared_abundances)) / (len(unique_abundances) + len(shared_abundances))
        
        if np.isnan(abund):
            err_msg = "Encountered nan values in avg equation"
            raise QuantMath_Error(message=err_msg)

        return(abund)

    #---------------------------------------------------------
    # See documentation for dNPAF equation, refers to using 
    # precursor abundance during quantification
    #---------------------------------------------------------
    def dNPAF(self, unique_a, shared_a, unique_lengths, shared_lengths):
        abund = 0.0

        # undefined behavior precautions
        if not unique_a:
            return(0.0)

        d = self.__dNPAF_dfactor(unique_a, unique_lengths, shared_lengths)

        sum_u = np.sum(unique_a)
        sum_s = np.sum(shared_a) * d
        sum_l = np.sum(unique_lengths) + np.sum(shared_lengths) 

        abund = (sum_u + sum_s) / sum_l

        if np.isnan(abund):
            err_msg = "Encountered nan values in dNPAF equation"
            raise QuantMath_Error(message=err_msg)

        return(abund)

    #-------------------------------------------------------------------------
    # See documentation for dNSAF equation, refers to using 
    # the number of unique and shared spectra (and their lengths) as opposed
    # to actual abundance values
    #-------------------------------------------------------------------------
    def dNSAF(self, unique_a, shared_a, unique_lengths, shared_lengths):
        abund = 0.0

        # Undefined behavior precautions
        if not unique_a and not shared_a:
            return 0.0

        num_unique_spectra = len(unique_a)
        num_shared_spectra = len(shared_a)

        d = self.__dNSAF_dfactor()

        n_top = num_unique_spectra + (d * num_shared_spectra)

        n_btm = np.sum(unique_lengths) + np.sum(shared_lengths)

        i_top = len(self.all_unique_abundances) + (d * len(self.all_shared_abundances))
        i_btm = np.sum(self.all_unique_lengths) + np.sum(self.all_shared_lengths)

        abund = (n_top / n_btm) / (i_top / i_btm)
        return(abund)

    #------------------------------------------------------------
    # The d factor for dNPAF apportions against all other 
    # peptides with unique abundances and lengths in the sample
    #------------------------------------------------------------
    def __dNPAF_dfactor(self, unique_a, unique_lengths, shared_lengths):
        d_factor = 0.0

        # if any of the params provided are 0, we return 0 to avoid undefined behavior
        # as the d factor equation will converge to 0 anyway
        if len(unique_a) == 0 or len(unique_lengths) == 0 or len(shared_lengths) == 0:
            return(0.0)

        sum_unique_abundances = np.sum(unique_a)
        apportioned_lengths = np.sum(shared_lengths) / np.sum(unique_lengths)

        i = np.sum(self.all_unique_abundances) * (np.sum(self.all_shared_lengths) / np.sum(self.all_unique_lengths))

        d_factor = (sum_unique_abundances * apportioned_lengths) / i

        if np.isnan(d_factor):
            err_msg = "Encountered nan values in dNPAF d factor equation"
            raise QuantMath_Error(message=err_msg)

        return(d_factor)

    #------------------------------------------------------
    # The d_factor for dNSAF simply apportions against all
    # other unique spectra in the sample
    #------------------------------------------------------
    def __dNSAF_dfactor(self):
        if len(self.all_unique_abundances) == 0:
            return 0.0
        else:
            return (1.0 / len(self.all_unique_abundances))

class QuantMath_Error(Exception):
    def __init__(self, message=None, stack_trace=None):
        if message:
            print(f"[QUANT MATH ERROR]: {message}", file=sys.stderr)
        else:
            print("\n[QUANT MATH ERROR]: Whoops, it looks like we ran into some undefined behavior..", file=sys.stderr)

        if stack_trace:
            print("Stack Trace: ", stack_trace)
        self.dev_msg()
        
    def dev_msg(self):
        msg = "If this continues to give you trouble,"
        msg += " please submit an issue at: \n https://gitlab.com/jackcampanella/quant"
        msg += "\nWith your stack trace and all information/files needed to replicate the issue."
        print(f"{msg}", file=sys.stderr)
            


        


        

    
