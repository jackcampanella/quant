''' 
  ____                 __    
 / __ \__ _____ ____  / /_   
/ /_/ / // / _ `/ _ \/ __/   
\___\_\_,_/\_,_/_//_/\__/    

| graph.py |

The Graph datastructure used by quant to propagate and retrieve information.
This implementation is referred to as a Bi-partite graph where the left set
of nodes represent the Peptides in the sample and the right set of nodes represent 
the proteins. Further, an edge is represented by a peptide to protein mapping.
'''
class PeptideNode:
    def __init__(self, sequence, abundance, protein=None, index=None):
        self.sequence = sequence
        self.abundance = abundance
        self.mapped_proteins = []
        self.index = index
        self.length = len(self.sequence)
        self.edges = []

class ProteinNode:
    def __init__(self, sequence, index=None, peptide=None, name=None, descr=None):
        self.sequence = sequence
        self.index = index
        self.peptide = peptide
        self.name = name
        self.description = descr
        self.length = len(sequence)
        self.edges = []
        self.computed_abundance = 0.0

        self.unique_abundances = []
        self.shared_abundances = []

        self.unique_lengths = []
        self.shared_lengths = []

    def get_abundance_range(self):
        min_abund = float("inf")
        max_abund = 0.0

        for a in self.unique_abundances:
            if a < min_abund and a != 0.0:
                min_abund = a

            if a > max_abund:
                max_abund = a

        for a in self.shared_abundances:
            if a < min_abund and a != 0.0:
                min_abund = a
            
            if a > max_abund:
                max_abund = a

        return([min_abund, max_abund])
        
class Edge:
    def __init__(self, left_index=None, right_index=None, index=None):
        self.index = index
        self.left_index = left_index
        self.right_index = right_index

class Graph:
    def __init__(self):
        self.peptide_nodes = []
        self.protein_nodes = []
        self.edges = []




