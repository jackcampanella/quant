# Quant
A proteomics tool that performs protein quantification using msms data and envelopes mapped to peptides. Specifically, a feature linker maps envelopes identified by segmentation to peptides/spectra identified by a search engine such as X!Tandem or Comet.

## Usage
----

### Run from source

```bash
python run_quant.py -f [file.json] -d [protein_database].fasta
```

### Docker
```bash 
docker run --rm -v /path/to/data:/home/quant/data --env file={} --env db={}
```

### Optional Arguments


some text

----
## Input file format
----
some text


## Protein Database format
----
some text


## Terminology
----
### NPAF & dNPAF
NPAF - Normalized Precursor Abundance Factor
dNPAF - Distributed Normalized Precursor Abundance Factor
This equation derived from SAF


### NSAF & dNSAF
NSAF - Normalized Spectral Abundance Factor
dNSAF - Distributed Normalized Spectral Abundance Factor
Article: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1672612/
