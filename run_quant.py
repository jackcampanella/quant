''' 
  ____                 __    
 / __ \__ _____ ____  / /_   
/ /_/ / // / _ `/ _ \/ __/   
\___\_\_,_/\_,_/_//_/\__/    

Proteomics tool that performs Protein Abundance estimation using peptide to envelope mappings

'''

import sys, os.path, traceback
import argparse
from quant import Quant, Quant_Error

def configure_arg_parser():
    parser = argparse.ArgumentParser(description='Tool to estimate Protein Abundances using peptide to envelope mappings')
    
    # Required arguments only include the input file
    required = parser.add_argument_group('Required Arguments') # Add type req's
    required.add_argument('-f', '--file', help='Json payload containing features with abundances/intensities', required=True)
    required.add_argument('-d', '--database', help='Protein Database in fasta format', required=True)

    # Optional arguments include abundance computation method and ability to provide output file name
    parser.add_argument('-m', '--method', nargs='?', const=1, type=str,
        default='pcr', 
        help='Computational method to estimate Proteins\' abundances. (Default) is pcr',
        choices=['pcr', 'spc', 'avg', 'sum'])
    
    parser.add_argument('-o', '--outfile_name', nargs='?', const=1, type=str,
        help='Specify output file name', default='quantification.json')

    return(parser)

def main(params):
    print(" | QUANT | Protein Quantification |")
    # Configuring arguments
    parser = configure_arg_parser()
    args = parser.parse_args()

    print("Using Abundance estimation method: ", args.method)

    # Ensure file exists before running Quant
    if os.path.isfile(args.file) and os.path.isfile(args.database):
        try:
            quant = Quant(args.file, args.database, args.method, args.outfile_name)
            quant.write_file()
        except Exception:
            stack_trace = traceback.format_exc()
            Quant_Error(stack_trace=stack_trace)
            exit()
    else:
        msg = "The file specified could not be read. Are you sure it exists?"
        Quant_Error(message=msg)
        exit()

if __name__ == "__main__":
    main(sys.argv[1:])