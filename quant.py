''' 
  ____                 __    
 / __ \__ _____ ____  / /_   
/ /_/ / // / _ `/ _ \/ __/   
\___\_\_,_/\_,_/_//_/\__/    

Proteomics tool that performs Protein Abundance estimation using peptide to envelope mappings
'''

import json
import sys, traceback
import os.path

from graph import PeptideNode, ProteinNode, Edge, Graph
from quant_math import QuantMath, QuantMath_Error

class Quant:
    def __init__(self, file, database, quant_method, outfile_name):
        self.file = file
        self.outfile_name = os.path.dirname(self.file) + '/' + outfile_name
        self.quant_method = quant_method
        self.database = database
        self.quant_math = QuantMath()
        self.abundance_calc_map = {
            'pcr': self.quant_math.dNPAF,
            'spc': self.quant_math.dNSAF,
            'avg': self.quant_math.avg_abundances,
            'sum': self.quant_math.sum_abundances
        }
        self.graph = Graph()
        self.__build_graph()
        self.__filter_peptides()
        self.__compute_abundances()

    #////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #                                          Primary Algorithm methods
    #------------------------------------------------------------------------------------------------------------
    
    #---------------------------------------------------------
    # Facilitates building the graph based off the input file
    #---------------------------------------------------------
    def __build_graph(self):
        self.__build_peptides()
        self.__build_proteins()
        self.__build_edges()

        print(f"...Identified {len(self.graph.peptide_nodes)} Peptides")
        print(f"...Identified {len(self.graph.protein_nodes)} Proteins in the sample")

    #----------------------------------------------------------
    # Computes protein's abundance in accordance to specified
    # quantification method
    #----------------------------------------------------------
    def __compute_abundances(self):
        # passing total number of proteins to quant math instance
        self.quant_math.total_proteins = len(self.graph.protein_nodes)
        
        # Retrieves the static function from QuantMath
        quant_calc_func = self.abundance_calc_map.get(self.quant_method)

        for prot_node in self.graph.protein_nodes:
            try:
                prot_node.computed_abundance = quant_calc_func(prot_node.unique_abundances, prot_node.shared_abundances, 
                                                prot_node.unique_lengths, prot_node.shared_lengths)
            except Exception:
                stack_trace = traceback.format_exc()
                QuantMath_Error(stack_trace=stack_trace)
                exit()


    #////////////////////////////////////////////////////////////////////////////////////////////////////////
    #                                   Graph Building helper methods
    #--------------------------------------------------------------------------------------------------------

    #--------------------------------------------------
    # Builds peptide nodes by extracting peptides 
    # and abundances in input file
    #--------------------------------------------------
    def __build_peptides(self):
        
        with open(self.file) as file:
            file_data = json.load(file)
            try:
                for feature in file_data:
                    pep_sequence = feature["peptide"]
                    abundance = feature["intensity"]
                    prot_name = feature["protein"]
                    
                    self.__store_peptide(pep_sequence, abundance, prot_name)
            
            except IndexError:
                stack_trace = traceback.format_exc()
                err_msg = "The input file supplied is not properly formatted. See documentation for file requirements."
                Quant_Error(message=err_msg, stack_trace=stack_trace)
                exit()

    #-----------------------------------------------
    # Builds protein nodes by extracting proteins
    # from protein database specified
    #-----------------------------------------------
    def __build_proteins(self):
        try:
            with open(self.database) as file:
                prot_name = ""
                prot_seq = ""

                for line in file:
                    # If we are at the header or on an empty line, this indicates
                    # that we have stored a protein's name and sequence
                    if ">" in line or len(line.strip()) == 0:

                        if prot_name:
                            # Store current protein and sequence
                            self.__store_protein(prot_seq, prot_name)
                    
                        # Resetting sequence
                        prot_seq = ""

                        # Moving onto next protein, removing first occurrence of carat
                        prot_name = line.replace(">", "", 1)
                    
                    # Indicates that we are on the sequence
                    else:
                        prot_seq += line
                
                # Accounting for final protein entry
                self.__store_protein(prot_seq, prot_name)
        
        except Exception:
            stack_trace = traceback.format_exc()
            err_msg = "The protein database is in an unsupported format, cannot process it."
            Quant_Error(message=err_msg, stack_trace=stack_trace)
            exit()

    #-------------------------------------------------------------------------
    # Builds edges from peptides to proteins via substring search of proteins 
    # in the database. Each node contains a list of type Edge indexes.
    #-------------------------------------------------------------------------
    def __build_edges(self):
        for pep_node in self.graph.peptide_nodes:
            for prot_node in self.graph.protein_nodes:
                if(pep_node.sequence in prot_node.sequence):
                    new_edge = Edge(index=len(self.graph.edges), left_index=pep_node.index, right_index=prot_node.index)
                    self.graph.edges.append(new_edge)
                    pep_node.edges.append(new_edge.index)
                    prot_node.edges.append(new_edge.index)

    #-------------------------------------------------------------------------
    # Separates degenerate and unique peptides and propagate abundance/length
    # information to mapped proteins accordingly
    #-------------------------------------------------------------------------
    def __filter_peptides(self):

        # Iterating through all peptides
        for pep_node in self.graph.peptide_nodes:
            # Iterating through all edges
            for edge_index in pep_node.edges:
                edge = self.graph.edges[edge_index]
                assert(edge.left_index == pep_node.index)

                # For degen peptides (they have more than one edge)
                if len(pep_node.edges) > 1: 
                    # Propagate to corresponding protein
                    self.__propagate_shared(edge.right_index, pep_node.abundance, pep_node.length)

                    # Also pass info to QuantMath instance
                    self.quant_math.all_unique_abundances.append(pep_node.abundance)
                    self.quant_math.all_unique_lengths.append(pep_node.length)

                # For unique peptides (they have exactly one edge)
                elif len(pep_node.edges) == 1:
                    # Propagate to corresponding protein
                    self.__propagate_unique(edge.right_index, pep_node.abundance, pep_node.length)

                    # Also pass info to QuantMath instance
                    self.quant_math.all_shared_abundances.append(pep_node.abundance)
                    self.quant_math.all_shared_lengths.append(pep_node.length)

                # all others are not mapped to proteins
                else:
                    print(f'[WARNING]: {pep_node.sequence} does not seem to have a mapped protein')
    
    #--------------------------------------------
    # Builds peptide node and stores it in graph
    #--------------------------------------------
    def __store_peptide(self, sequence, abundance, protein):
        pep_node = PeptideNode(sequence.strip(), abundance)
        pep_node.mapped_proteins.append(protein)
        pep_node.index = len(self.graph.peptide_nodes)

        self.graph.peptide_nodes.append(pep_node)

    #---------------------------------------------------
    #   Builds protein node and stores it in graph
    #---------------------------------------------------
    def __store_protein(self, sequence, header):
        name, descr = self.__parse_header(header)

        prot_node = ProteinNode(sequence=sequence.strip(), name=name, descr=descr)
        prot_node.index = len(self.graph.protein_nodes)

        self.graph.protein_nodes.append(prot_node)
    
    #--------------------------------------------------------------------
    #   Separates the protein name from the description where applicable
    #--------------------------------------------------------------------
    def __parse_header(self, header):
        prot_name = None
        prot_descr = None

        if len(header.split(" ", 1)) == 1:
            prot_name = header.strip()

        elif len(header.split(" ", 1)) == 2:
            prot_name, prot_descr = header.split(" ", 1)
        
        if prot_name is not None: prot_name = prot_name.strip()
        if prot_descr is not None: prot_descr = prot_descr.strip()
        
        return(prot_name, prot_descr)

    #/////////////////////////////////////////////////////////////////////////////////////////////////
    #                                Helper propagation methods
    #-------------------------------------------------------------------------------------------------

    #----------------------------------------------------------------------------
    # Propagates information of shared peptides to specified mapped protein node
    #----------------------------------------------------------------------------
    def __propagate_shared(self, protein_index, pep_abundance, pep_length):
        self.graph.protein_nodes[protein_index].shared_abundances.append(pep_abundance)
        self.graph.protein_nodes[protein_index].shared_lengths.append(pep_length)

    #----------------------------------------------------------------------------
    # Propagates information of unique peptides to specified mapped protein node
    #----------------------------------------------------------------------------
    def __propagate_unique(self, protein_index, pep_abundance, pep_length):
        self.graph.protein_nodes[protein_index].unique_abundances.append(pep_abundance)
        self.graph.protein_nodes[protein_index].unique_lengths.append(pep_length)

    #/////////////////////////////////////////////////////////////////////////////////////////////////
    #                              File processing helper methods
    #-------------------------------------------------------------------------------------------------
    
    #------------------------------------------
    # Writes results to a json formatted file
    #------------------------------------------
    def write_file(self):
        results = []
        for prot_node in self.graph.protein_nodes:
            if len(prot_node.edges) > 0:
                # Retrieving abundance range for this protein
                abund_range = prot_node.get_abundance_range()

                # Constructing protein object
                current_protein = {
                    "protein": prot_node.name,
                    "abundance": prot_node.computed_abundance,
                    "min_abundance": abund_range[0],
                    "max_abundance": abund_range[1],
                    "num_connected_peptides": len(prot_node.edges)
                }
                results.append(current_protein)
        
        print(f" => Computed abundances for {len(results)} proteins")
        
        with open(self.outfile_name, "w") as outfile:
            json.dump(results, outfile, indent=2)

#/////////////////////////////////////////////////////////////////////////////////////////////////
#                              Quant Error Class
#-------------------------------------------------------------------------------------------------
class Quant_Error(Exception):
    def __init__(self, message=None, stack_trace=None):
        if message:
            print(f"[QUANT ERROR]: {message}", file=sys.stderr)
        else:
            print("\n[QUANT ERROR]: Whoops, it looks like we ran into some undefined behavior..", file=sys.stderr)

        if stack_trace:
            print("Stack Trace: ", stack_trace)
        self.dev_msg()
        
    def dev_msg(self):
        msg = "If this continues to give you trouble,"
        msg += " please submit an issue at: \n https://gitlab.com/jackcampanella/quant"
        msg += "\nWith your stack trace and all information/files needed to replicate the issue."
        print(f"{msg}", file=sys.stderr)
        






